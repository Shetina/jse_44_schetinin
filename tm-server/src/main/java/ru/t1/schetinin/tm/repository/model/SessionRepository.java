package ru.t1.schetinin.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.schetinin.tm.api.repository.model.ISessionRepository;
import ru.t1.schetinin.tm.model.Session;

import javax.persistence.EntityManager;

public class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public Class<Session> getEntity() {
        return Session.class;
    }

}