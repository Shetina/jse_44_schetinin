package ru.t1.schetinin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.api.repository.dto.IDTORepository;
import ru.t1.schetinin.tm.comparator.CreatedComparator;
import ru.t1.schetinin.tm.comparator.StatusComparator;
import ru.t1.schetinin.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractDTORepository<M extends AbstractModelDTO> implements IDTORepository<M> {

    @NotNull
    protected final EntityManager entityManager;

    public AbstractDTORepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        if (comparator == StatusComparator.INSTANCE) return "status";
        else return "name";
    }

    @NotNull
    public abstract Class<M> getEntity();

    @NotNull
    @Override
    public M add(@NotNull final M model) throws Exception {
        entityManager.persist(model);
        return model;
    }

    @Override
    public void clear() throws Exception {
        @NotNull final String jpql = "DELETE FROM " + getEntity().getSimpleName() + " m";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    public boolean existsById(@NotNull final String id) throws Exception {
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    public List<M> findAll() throws Exception {
        @NotNull final String jpql = "SELECT m FROM " + getEntity().getSimpleName() + " m";
        return entityManager.createQuery(jpql, getEntity()).getResultList();
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final Comparator comparator) throws Exception {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<M> criteriaQuery = criteriaBuilder.createQuery(getEntity());
        @NotNull final Root<M> from = criteriaQuery.from(getEntity());
        criteriaQuery.select(from);
        criteriaQuery.orderBy(criteriaBuilder.asc(from.get(getSortType(comparator))));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String id) throws Exception {
        @NotNull final String jpql = "SELECT m FROM " + getEntity().getSimpleName() + " m WHERE m.id = :id";
        return entityManager.createQuery(jpql, getEntity())
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream().findAny().orElse(null);
    }

    @Override
    public int getSize() throws Exception {
        @NotNull final String jpql = "SELECT COUNT(m) FROM " + getEntity().getSimpleName() + " m";
        return entityManager.createQuery(jpql, Long.class).getSingleResult().intValue();
    }

    @Override
    public void remove(@NotNull final M model) throws Exception {
        entityManager.remove(model);
    }

    @Override
    public void update(@NotNull final M model) throws Exception {
        entityManager.merge(model);
    }

}