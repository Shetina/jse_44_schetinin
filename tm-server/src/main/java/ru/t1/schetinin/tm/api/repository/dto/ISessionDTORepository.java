package ru.t1.schetinin.tm.api.repository.dto;

import ru.t1.schetinin.tm.dto.model.SessionDTO;

public interface ISessionDTORepository extends IUserOwnedDTORepository<SessionDTO> {
}