package ru.t1.schetinin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.enumerated.Role;
import ru.t1.schetinin.tm.dto.model.UserDTO;

public interface IUserService extends IService<UserDTO> {

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    @NotNull
    UserDTO findByEmail(@Nullable String email);

    @Nullable
    UserDTO removeOne(@Nullable UserDTO model);

    @Nullable
    UserDTO removeByLogin(@Nullable String login);

    @Nullable
    UserDTO removeByEmail(@Nullable String email);

    @NotNull
    UserDTO setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    UserDTO updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

    Boolean isLoginExist(@Nullable String login);

    Boolean isEmailExist(@Nullable String email);

    UserDTO lockUserByLogin(@Nullable String login);

    UserDTO unlockUserByLogin(@Nullable String login);

}