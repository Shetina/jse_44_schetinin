package ru.t1.schetinin.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.enumerated.Role;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "session_table")
public class SessionDTO extends AbstractUserOwnedModelDTO {

    @Column
    @NotNull
    private Date date = new Date();

    @Column
    @Nullable
    @Enumerated(EnumType.STRING)
    private Role role = null;

}